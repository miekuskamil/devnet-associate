#!/usr/bin/python3
#from . import credentials
import os.path
import yaml
import pprint
import requests
import json
import logging
import urllib3
import os
import pytest

#test


#read env variables
USER = os.getenv('USERNAME_ACI')
PASSWORD = os.environ.get('PASSWORD_ACI')


#disable SSL warning
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

#setup logger
logger = logging.getLogger(__name__)


@pytest.fixture()
def login_initial(username=USER, password=PASSWORD):
    '''
    Test for login return code
    :param username: str > username
    :param password: str > password
    :return: str > connection 1
    '''
    logger.info('Fixture login')
    url1 = 'https://sandboxapicdc.cisco.com/api/aaaLogin.json'
    body = json.dumps({ "aaaUser" : { "attributes": {"name":username,"pwd":password } } })
    connection1 = requests.post(url1, data=body, verify=False)
    return [connection1]


@pytest.fixture()
def cookie_subsequent(username=USER, password=PASSWORD):
    '''
    Test for login return code and cookie existience
    :param username: str > username
    :param password: str > password
    :return: dict > lldp neighbors
    '''
    ###initial login
    logger.info('Fixture Cookie')
    url1 = 'https://sandboxapicdc.cisco.com/api/aaaLogin.json'
    body = json.dumps({ "aaaUser" : { "attributes": {"name":username,"pwd":password } } })
    connection1 = requests.post(url1, data=body, verify=False)
 
    ###cookie extract
    post_connection1_json = json.loads(connection1.text)
    login_attributes = post_connection1_json['imdata'][0]['aaaLogin']['attributes']
    cookies = {}
    cookies['APIC-Cookie'] = login_attributes['token']
 
    ###lldp neighbors extract with cookie login
    url2 = 'https://sandboxapicdc.cisco.com/api/node/class/fvCEp.json'
    lldp = requests.get(url2, cookies=cookies, verify = False )
    post_lldp_json = json.loads(lldp.text)
    return [lldp, post_lldp_json]


###rewrite here to pass the list of all files in dir and see the match for ap, epg etc.
def test_file_existience():
    logger.info('Test file existence')
    assert os.path.exists('implementation/roles/ap/vars/main.yml')
    assert os.path.exists('implementation/roles/epg/vars/main.yml')


def test_login(login_initial):
    '''
    Test for login return code
    '''
    logger.info('Test Initial Login')
    assert (login_initial[0].status_code == 200 or login_initial[0].status_code == 202)


def test_cookie(cookie_subsequent):
    '''
    Test for login return code and cookie existience
    '''
    logger.info('Test cookie login')
    ###testing cookie lldp neighbors extract
    assert cookie_subsequent != []
    assert (cookie_subsequent[0].status_code == 200 or cookie_subsequent[0].status_code == 202)
    ###test for specific fields:
    for endpoints in cookie_subsequent[1]['imdata']:
        assert ("fvCEp" in endpoints.keys())


def test_ap_prof():
    '''
    Test for var syntax on ap profile
    :return: no return
    '''
    logger.info('Test AP profile syntax')
    with open('implementation/roles/ap/vars/main.yml') as f:
        data = yaml.load(f, Loader=yaml.Loader)
        assert data != {}
        for k, v in data.items():
            pprint.pprint(list(v))
            for i in v:
                assert (i.get('tenant') == 'common' and i.get('description') == 'default') ### check if dict values equal to common/default
                assert "ap_" in str(i.values())   ### check if ap_ prefix in dict 
                assert "abc" not in str(i.values())   ### check if abc in dict 
                assert ("name" in i.keys())   ### check if name in dict keys      
                #test       